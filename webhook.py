from flask import Flask,request,jsonify
import os

app = Flask(__name__)

@app.route("/",methods=["GET"])

def index():
    if request.args.get("hub.mode") == "subscribe" and request.args.get("hub.challenge"):
        if not request.args.get("hub.verify_token") == "verify_me":
            return "Verification token mismatch", 403
        return request.args["hub.challenge"], 200

    return "Hello world", 200


if __name__=="__main__":
    app.run()
